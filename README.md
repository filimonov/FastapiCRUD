# FastapiCRUD service

## Introduction
This is a RESTful service to show authorized employees their salary and its increase date. Service includes some methods to manage employees (adding, updating and removing). Authentication is based on OAuth2 with JWT so passwords are stored securely.

## Specifications
The project is dockerized and works through `docker compose`, dependencies are fixed with `poetry`. The service runs on `FastAPI` in `asynchronous mode` with `PostgreSQL` as a database and `uvicorne` as a web server. Migrations are managed by `alembic`. Testing is implemented using `pytest`.

## Quickstart
First of all, clone the repository to your local machine

```bash
git clone https://gitlab.com/filimonov/FastapiCRUD.git
```

To build docker image and run server you should open a terminal/powershell and go to the repository

```bash
cd ./path/to/the/repository
```

Run command

```bash
docker compose up -d
```
Your service is now available and you can open openapi docs at `http://127.0.0.1:8001/docs`

## Testing
To run the tests, you should find your container ID with this command

```bash
docker ps
```

Your stdout will look like this

```bash
CONTAINER ID   IMAGE                                COMMAND                  CREATED          STATUS          PORTS                    NAMES
8aeea08879a6   fastapicrud-salary_monitor_service   "bash -c 'poetry run…"   15 minutes ago   Up 15 minutes   0.0.0.0:8001->8000/tcp   fastapicrud-salary_monitor_service-1
7b14f6ed6797   postgres:15.3                        "docker-entrypoint.s…"   15 minutes ago   Up 15 minutes   0.0.0.0:5432->5432/tcp   fastapicrud-employee_db-1
```

With this we can run our tests

```bash
docker exec -it 8aeea08879a6 pytest -vv
#-i flag is --interactive ("Keep stdin open even if not attached") and -t is --tty (" Allocate a pseudo-TTY"); the -vv flag is used for more information
```

You should see something like this

```bash
================================================= test session starts ==================================================
platform linux -- Python 3.10.12, pytest-7.3.1, pluggy-1.0.0 -- /usr/local/bin/python
cachedir: .pytest_cache
rootdir: /salary_monitor
plugins: asyncio-0.21.0, anyio-3.7.0
asyncio: mode=strict
collected 11 items

tests/test_users.py::TestCRUD::test_add_employee PASSED                                                          [  9%]
tests/test_users.py::TestCRUD::test_add_employee_invalid_json PASSED                                             [ 18%]
tests/test_users.py::TestCRUD::test_update_employee PASSED                                                       [ 27%]
tests/test_users.py::TestCRUD::test_update_note_invalid[1-payload0-422] PASSED                                   [ 36%]
tests/test_users.py::TestCRUD::test_update_note_invalid[1-payload1-422] PASSED                                   [ 45%]
tests/test_users.py::TestCRUD::test_update_note_invalid[999-payload2-404] PASSED                                 [ 54%]
tests/test_users.py::TestCRUD::test_delete_employee PASSED                                                       [ 63%]
tests/test_users.py::TestCRUD::test_delete_employee_incorrect_id PASSED                                          [ 72%]
tests/test_users.py::TestAuthentication::test_user_can_login_and_get_valid_token PASSED                          [ 81%]
tests/test_users.py::TestAuthentication::test_login_with_invalid_password PASSED                                 [ 90%]
tests/test_users.py::TestAuthentication::test_user_with_token_can_get_his_data PASSED                            [100%]

================================================== 11 passed in 1.78s ==================================================
```

## Customization
You can replace `POSTGRES_USER`, `POSTGRES_PASSWORD`, `POSTGRES_DB`, and `DATABASE_URI` with your own values in `docker-compose.yml`, but if you've already built the image, you'll need to rebuild it with the new values.

## Security
You can be sure that your users' passwords are safe because the access token secret key is generated on your local machine the first time you run `docker compose up` and is individual for each image.

## Project structure

```
FASTAPICRUD
│   .gitignore
│   docker-compose.yml
│   generate_key.sh #script to generate the token secret key for the first run
│   README.md
│
└───salary_monitor_service
    │   alembic.ini
    │   Dockerfile
    │   poetry.lock
    │   pyproject.toml
    │   README.md
    │
    ├───migrations
    │   │   env.py
    │   │   README
    │   │   script.py.mako
    │   │
    │   └───versions
    │       4c6ef63c4a6c_test_migration.py
    │       6404b7f137d7_init_migration.py
    │
    ├───salary_monitor
    │   │   main.py
    │   │   __init__.py
    │   │
    │   └───api
    │       auth.py
    │       db.py
    │       db_manager.py
    │       employees.py
    │       models.py
    │       __init__.py
    │
    └───tests
            conftest.py
            test_auth.py
            test_crud.py
            __init__.py
```
