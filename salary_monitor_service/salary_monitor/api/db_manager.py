from .db import database, employees
from .models import EmployeeIn


async def get_employee_by_id(id):
    query = employees.select().where(employees.c.id == id)
    return await database.fetch_one(query=query)


async def get_employee_by_username(username):
    query = employees.select().where(employees.c.username == username)
    return await database.fetch_one(query=query)


async def get_all_employees():
    query = employees.select()
    return await database.fetch_all(query=query)


async def add_employee(data: EmployeeIn):
    query = employees.insert().values(**data.dict())
    return await database.execute(query=query)


async def update_employee(id: int, data: EmployeeIn):
    query = (
        employees
        .update()
        .where(employees.c.id == id)
        .values(**data.dict())
    )
    return await database.execute(query=query)


async def delete_employee(id: int):
    query = employees.delete().where(employees.c.id == id)
    return await database.execute(query=query)
