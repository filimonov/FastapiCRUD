import os

from databases import Database

from sqlalchemy import (Boolean, Column, Date, Float, Integer, MetaData, String, Table, create_engine)

DATABASE_URL = os.getenv('DATABASE_URI')
engine = create_engine(DATABASE_URL)

metadata = MetaData()

employees = Table(
    'employees',
    metadata,
    Column('id', Integer, primary_key=True, autoincrement=True),
    Column('username', String(200)),
    Column('full_name', String(200)),
    Column('salary', Float),
    Column('increase_date', Date),
    Column('password', String(200)),
    Column('disabled', Boolean, default=False),
)

database = Database(DATABASE_URL)
