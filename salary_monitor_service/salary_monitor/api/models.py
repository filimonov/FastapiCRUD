import datetime
from typing import Optional

from pydantic import BaseModel


class EmployeeIn(BaseModel):
    username: str
    full_name: str | None = None
    salary: int
    increase_date: datetime.date
    password: str


class EmployeeOut(EmployeeIn):
    id: int
    disabled: bool | None = None


class EmployeeView(BaseModel):
    full_name: str | None = None
    salary: int
    increase_date: datetime.date


class EmployeeUpdate(EmployeeIn):
    username: Optional[str] = None
    full_name: Optional[str] = None
    salary: Optional[int] = None
    increase_date: Optional[datetime.date] = None
    password: Optional[str] = None


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str | None = None
