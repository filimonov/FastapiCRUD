from typing import Annotated

from fastapi import APIRouter, Depends, HTTPException

from . import db_manager
from .auth import get_current_active_user, get_password_hash
from .models import EmployeeIn, EmployeeView

employees = APIRouter()


@employees.get('/me', response_model=EmployeeView)
async def show_employee(current_user: Annotated[EmployeeIn, Depends(get_current_active_user)]):
    return current_user


@employees.post('/signup', status_code=201)
async def add_employee(data: EmployeeIn):
    data.password = get_password_hash(data.password)
    employee_id = await db_manager.add_employee(data)
    response = {
        'id': employee_id,
        **data.dict(),
    }

    return response


@employees.put('/{id}')
async def update_employee(id: int, data: EmployeeIn):
    employee = await db_manager.get_employee_by_id(id)
    if not employee:
        raise HTTPException(status_code=404, detail='Employee not found')

    data.password = get_password_hash(data.password)
    update_data = data.dict(exclude_unset=True)
    employee_in_db = EmployeeIn(**employee)

    updated_employee = employee_in_db.copy(update=update_data)

    return await db_manager.update_employee(id, updated_employee)


@employees.delete('/{id}')
async def delete_employee(id: int):
    employee = await db_manager.get_employee_by_id(id)
    if not employee:
        raise HTTPException(status_code=404, detail='Employee not found')
    return await db_manager.delete_employee(id)
