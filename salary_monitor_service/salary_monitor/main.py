from fastapi import FastAPI

from .api.auth import auth
from .api.db import database, engine, metadata
from .api.employees import employees

metadata.create_all(engine)

app = FastAPI()


@app.on_event("startup")
async def startup():
    if not database.is_connected:
        await database.connect()


@app.on_event("shutdown")
async def shutdown():
    if database.is_connected:
        await database.disconnect()

app.include_router(auth, prefix='/api/v1/auth', tags=['auth'])
app.include_router(employees, prefix='/api/v1/employees', tags=['employees'])
