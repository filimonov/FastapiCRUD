from fastapi.testclient import TestClient

import pytest

from salary_monitor.main import app


@pytest.fixture(scope='module')
def client():
    client = TestClient(app)
    yield client
