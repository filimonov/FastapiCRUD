import json

import pytest

from salary_monitor.api import db_manager


class TestCRUD():
    def test_add_employee(self, client, monkeypatch):
        data = {
            "username": "johndoe2",
            "full_name": "John Doe2",
            "salary": 55000,
            "increase_date": "2023-08-27",
            "password": "secret2",
        }

        response_data = {
            "id": 1,
            "username": "johndoe2",
            "full_name": "John Doe2",
            "salary": 55000,
            "increase_date": "2023-08-27",
        }

        async def mock_function(payload):
            return 1

        monkeypatch.setattr(db_manager, 'add_employee', mock_function)

        response = client.post('api/v1/employees/signup', content=json.dumps(data))

        assert response.status_code == 201
        assert response.json()['id'] == response_data['id'], 'The received id is different from the expected one.'
        assert response.json()['username'] == response_data['username'], 'The received username is different from the expected one.'
        assert response.json()['full_name'] == response_data['full_name'], 'The received full name is different from the expected one.'
        assert response.json()['salary'] == response_data['salary'], 'The received salary is different from the expected one.'
        assert response.json()['increase_date'] == response_data['increase_date'], 'The received increase date is different from the expected one.'
        # We can't get the same password hash, so we won't compare it

    def test_add_employee_invalid_json(self, client):
        response = client.post('api/v1/employees/signup', content=json.dumps({'wrong': 'request'}))
        assert response.status_code == 422

    def test_update_employee(self, client, monkeypatch):
        mock_data = {
            'id': 1,
            'username': 'johndoe',
            'full_name': 'John Doe',
            'salary': '50000',
            'increase_date': '2023-08-26',
            'password': '$2b$12$clonU13oDJkXOMSARC7d8u2h9XTA5yHoLMQBrODhuANE7O53MUcOO',
        }

        mock_update_data = {
            'id': 1,
            'username': 'johndoe',
            'full_name': 'John Doe2',
            'salary': '50000',
            'increase_date': '2023-08-26',
            'password': '$2b$12$clonU13oDJkXOMSARC7d8u2h9XTA5yHoLMQBrODhuANE7O53MUcOO',
        }

        async def mock_get(id):
            return mock_data

        monkeypatch.setattr(db_manager, "get_employee_by_id", mock_get)

        async def mock_put(id, payload):
            return

        monkeypatch.setattr(db_manager, "update_employee", mock_put)

        response = client.put("api/v1/employees/1/", content=json.dumps(mock_update_data))
        assert response.status_code == 200
        assert response.json() is None

    @pytest.mark.parametrize(
        "id, payload, status_code",
        [
            [1, {}, 422],
            [1, {"description": "bar"}, 422],
            [999, {
                    'id': 2,
                    'username': 'johndoe',
                    'full_name': 'John Doe2',
                    'salary': '50000',
                    'increase_date': '2023-08-26',
                    'password': '$2b$12$clonU13oDJkXOMSARC7d8u2h9XTA5yHoLMQBrODhuANE7O53MUcOO',
                  }, 404],
        ],
    )
    def test_update_note_invalid(self, client, monkeypatch, id, payload, status_code):
        async def mock_get(id):
            return None

        monkeypatch.setattr(db_manager, "get_employee_by_id", mock_get)

        response = client.put(f"api/v1/employees/{id}/", content=json.dumps(payload))
        assert response.status_code == status_code

    def test_delete_employee(self, client, monkeypatch):
        mock_data = {
            'id': 1,
            'username': 'johndoe',
            'full_name': 'John Doe',
            'salary': '50000',
            'increase_date': '2023-08-26',
            'password': '$2b$12$clonU13oDJkXOMSARC7d8u2h9XTA5yHoLMQBrODhuANE7O53MUcOO',
        }

        async def mock_get(id):
            return mock_data

        monkeypatch.setattr(db_manager, "get_employee_by_id", mock_get)

        async def mock_delete(id):
            return id

        monkeypatch.setattr(db_manager, "delete_employee", mock_delete)

        response = client.delete("api/v1/employees/1/")
        assert response.status_code == 200
        assert response.json() == 1

    def test_delete_employee_incorrect_id(self, client, monkeypatch):
        async def mock_get(id):
            return None

        monkeypatch.setattr(db_manager, "get_employee_by_id", mock_get)

        response = client.delete("api/v1/employees/999/")
        assert response.status_code == 404
        assert response.json()["detail"] == "Employee not found"
