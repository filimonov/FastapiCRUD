from datetime import timedelta

from salary_monitor.api import db_manager
from salary_monitor.api.auth import create_access_token

ACCESS_TOKEN_EXPIRE_MINUTES = 30


class TestAuthentication:
    def test_user_can_login_and_get_valid_token(self, client, monkeypatch):
        data = {
            'username': 'johndoe',
            'password': 'secret',
        }

        mock_user_data = {
            'id': 4,
            'username': 'johndoe',
            'full)name': 'John Doe',
            'salary': '50000',
            'increase_date': '2023-08-26',
            'password': '$2b$12$clonU13oDJkXOMSARC7d8u2h9XTA5yHoLMQBrODhuANE7O53MUcOO',
        }

        async def mock_function(payload):
            return mock_user_data

        monkeypatch.setattr(db_manager, 'get_employee_by_username', mock_function)

        response = client.post('api/v1/auth/token', data=data)

        assert response.status_code == 200
        assert response.json()['token_type'] == 'bearer'
        assert response.json()['access_token'] is not None, 'Access token is not exist'

    def test_login_with_invalid_password(self, client, monkeypatch):
        data = {
            'username': 'johndoe',
            'password': 'wrong',
        }

        mock_user_data = {
            'id': 4,
            'username': 'johndoe',
            'full)name': 'John Doe',
            'salary': '50000',
            'increase_date': '2023-08-26',
            'password': '$2b$12$clonU13oDJkXOMSARC7d8u2h9XTA5yHoLMQBrODhuANE7O53MUcOO',
        }

        async def mock_function(payload):
            return mock_user_data

        monkeypatch.setattr(db_manager, 'get_employee_by_username', mock_function)

        response = client.post('api/v1/auth/token', data=data)

        assert response.status_code == 401
        assert response.json()['detail'] == 'Incorrect username or password'

    def test_user_with_token_can_get_his_data(self, client, monkeypatch):
        token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
        token = create_access_token(data={'sub': 'johndoe'}, expires_delta=token_expires)

        mock_user_data = {
            'id': 4,
            'username': 'johndoe',
            'full_name': 'John Doe',
            'salary': '50000',
            'increase_date': '2023-08-26',
            'password': '$2b$12$clonU13oDJkXOMSARC7d8u2h9XTA5yHoLMQBrODhuANE7O53MUcOO',
        }

        async def mock_function(payload):
            return mock_user_data

        monkeypatch.setattr(db_manager, 'get_employee_by_username', mock_function)

        headers = {
            'Authorization': f'Bearer {token}',
        }

        response = client.get('api/v1/employees/me', headers=headers)

        assert response.status_code == 200
        assert response.json()['full_name'] == 'John Doe', 'Wrong full name'
        assert response.json()['salary'] == 50000, 'Wrong salary'
        assert response.json()['increase_date'] == '2023-08-26', 'Wrong increase date'
